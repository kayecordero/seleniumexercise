package com.elavon.exerciseSeven;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Exercise7 {
	
	WebDriver driver;
	
	public WebDriver setupWebDriver(String browser)  {
		if (browser == "firefox") {
//			 System.setProperty("webdriver.firefox.driver",
//			 "geckodriver.exe");
//			 driver = new FirefoxDriver();
			FirefoxDriverManager.getInstance().setup();
			DesiredCapabilities capabilities=DesiredCapabilities.firefox();
			System.out.println("Debug 2");
//			capabilities.setCapability("marionette", true);
			System.out.println("Debug 3");
			driver = new FirefoxDriver();
			System.out.println("Debug 4");
		} else if (browser == "chrome") {
			// System.setProperty("webdriver.chrome.driver",
			// "chromedriver.exe");
			ChromeDriverManager.getInstance().setup();
			driver = new ChromeDriver();
			// ChromeDriverManager.getInstance().setup();
		} else if (browser == "iexplorer") {
			// System.setProperty("webdriver.ie.driver",
			// "IEDriverServer_Win32.exe");
			InternetExplorerDriverManager.getInstance().setup();
			DesiredCapabilities ieCapabilities = DesiredCapabilities
					.internetExplorer();
			ieCapabilities
					.setCapability(
							InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
			driver = new InternetExplorerDriver();
			// default browser indicate here
		} else {
			InternetExplorerDriverManager.getInstance().setup();
			DesiredCapabilities ieCapabilities = DesiredCapabilities
					.internetExplorer();
			ieCapabilities
					.setCapability(
							InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
			driver = new InternetExplorerDriver();
		}

		return driver;

	}

	@Before
	public void setupTest() {
		driver = setupWebDriver("chrome");
	}
	
	@Test
	public void testcase1(){
		String baseUrl = "http://demoqa.com/";
		driver.get(baseUrl);
		WebElement registration = driver.findElement(By.id("menu-item-374"));
		registration.click();
		driver.findElement(By.id("name_3_firstname")).sendKeys("Kaye");
		driver.findElement(By.id("name_3_lastname")).sendKeys("Cordero");
		WebElement maritalStat = driver.findElement(By.cssSelector("[name='radio_4[]'][value='single']"));
		maritalStat.click();
		WebElement hobby = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='reading']"));
		hobby.click();
		WebElement selectCountry = driver.findElement(By.id("dropdown_7"));
		Select dropdownCountry = new Select(selectCountry);
		dropdownCountry.selectByVisibleText("South Korea");
		//Date of Birth
		WebElement month = driver.findElement(By.id("mm_date_8"));
		Select dropdownMonth = new Select(month);
		dropdownMonth.selectByVisibleText("1");
		WebElement day = driver.findElement(By.id("dd_date_8"));
		Select dropdownDay = new Select(day);
		dropdownDay.selectByVisibleText("20");
		WebElement year = driver.findElement(By.id("yy_date_8"));
		Select dropdownYear = new Select(year);
		dropdownYear.selectByVisibleText("1997");
		
		driver.findElement(By.id("phone_9")).sendKeys("09123456789");
		driver.findElement(By.id("username")).sendKeys("okaye");
		driver.findElement(By.id("email_1")).sendKeys("dummy@yahoo.com");
		
		WebElement chooseProfile = driver.findElement(By.id("profile_pic_10"));
		chooseProfile.sendKeys("C:\\Users\\KPCordero\\picture.jpg");
		driver.findElement(By.id("description")).sendKeys("Hello Iam Kaye! Im awesome!");
		driver.findElement(By.id("password_2")).sendKeys("**2G%Ae8586#**");
		driver.findElement(By.id("confirm_password_password_2")).sendKeys("**2G%Ae8586#**");
		
		WebElement submit = driver.findElement(By.cssSelector("[name='pie_submit'][value='Submit']"));
		submit.click();
		driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
		driver.close();		
	}
		@Test
		public void testcase2(){
		String baseUrl = "http://demoqa.com/";
		driver.get(baseUrl);
		WebElement registration = driver.findElement(By.id("menu-item-374"));
		registration.click();
		System.out.println("PAGE TITLE: "+driver.getTitle());
		driver.findElement(By.id("name_3_firstname")).sendKeys("Kaye");
		driver.findElement(By.id("name_3_lastname")).sendKeys("Cordero");
		WebElement maritalStat = driver.findElement(By.cssSelector("[name='radio_4[]'][value='single']"));
		maritalStat.click();
		WebElement hobby = driver.findElement(By.cssSelector("[name='checkbox_5[]'][value='reading']"));
		hobby.click();
		WebElement selectCountry = driver.findElement(By.id("dropdown_7"));
		Select dropdownCountry = new Select(selectCountry);
		dropdownCountry.selectByVisibleText("South Korea");
		//Date of Birth
		WebElement month = driver.findElement(By.id("mm_date_8"));
		Select dropdownMonth = new Select(month);
		dropdownMonth.selectByVisibleText("1");
		WebElement day = driver.findElement(By.id("dd_date_8"));
		Select dropdownDay = new Select(day);
		dropdownDay.selectByVisibleText("20");
		WebElement year = driver.findElement(By.id("yy_date_8"));
		Select dropdownYear = new Select(year);
		dropdownYear.selectByVisibleText("1997");
		
		driver.findElement(By.id("phone_9")).sendKeys("09123456789");
		driver.findElement(By.id("username")).sendKeys("okaye");
		driver.findElement(By.id("email_1")).sendKeys("dummy@yahoo.com");
		
		WebElement chooseProfile = driver.findElement(By.id("profile_pic_10"));
		chooseProfile.sendKeys("C:\\Users\\KPCordero\\picture.jpg");
		driver.findElement(By.id("description")).sendKeys("Hello Iam Kaye! Im awesome!");
		driver.findElement(By.id("password_2")).sendKeys("**2G%Ae8586#**");
		driver.findElement(By.id("confirm_password_password_2")).sendKeys("**2G%Ae8586#**");
		
		WebElement submit = driver.findElement(By.cssSelector("[name='pie_submit'][value='Submit']"));
		submit.click();
		driver.close();
		}
	}


